'use strict';

const Tween = require('gsap');

const $$ = require('./fauxjq');

/*
 */
function Gallery(opts) {
  this.insertImage = this.insertImage.bind(this);
  this.imageCount = 0;
  this.opts = opts || {};
}

Gallery.prototype = {
  constructor: Gallery,

  insertImage: function(imageObj, data) {
    this.imageCount++;
    let opts = this.opts;

    let outerDiv = $$.createEl('div');
    outerDiv.className = `${opts.xs} ${opts.sm} ${opts.md} ${opts.lg}`;

    let div = $$.createEl('div');
    div.className = 'img-container';

    let heading = $$.createEl('h3');
    heading.innerHTML = data.name;
    div.appendChild(heading);

    div.appendChild(imageObj);
    outerDiv.appendChild(div);

    // DESCRIPTION TO CONTAINER
    let p = $$.createEl('p');
    p.innerHTML = data.description;
    div.appendChild(p);

    // IMAGE TO GALLERY
    $$('.row.gallery').appendTo(outerDiv);

    $$(outerDiv)
      // SHOW
      .on('mouseover', function() {
        let p = this.querySelectorAll('p')[0];
        $$(p).css({
          visibility: 'visible',
          'opacity': 1
        });
        Tween.to(p, 0.1, { bottom: '20px', ease: Expo.easeIn });
        Tween.to(heading, 0.5, { 'opacity': '1', left: '50px', ease: Expo.easeOut });
      })

      // HIDE
      .on('mouseout', function() {
        let p = this.querySelectorAll('p')[0];
        $$(p).css({
          visibility: 'hidden',
          'opacity': 0
        });
        Tween.to(p, 0.1, { bottom: '50px', ease: Expo.easeIn });
        Tween.to(heading, 0.5, { 'opacity': '0', left:'0', ease: Expo.easeOut });
      });
  }
};

module.exports = Gallery;