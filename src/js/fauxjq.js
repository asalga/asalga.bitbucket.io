'use strict';

/*
  Faux JQuery
*/

function FauxJQ(el) {
  
  if (this !== window &&  this !== undefined) {
    console.log('>>>', this);
    this.el = (typeof el === 'object') ? el : document.querySelectorAll(el);

    this.on = function(evt, func) {
      if (typeof this.el.length === 'undefined') {
        this.el.addEventListener(evt, func);
      } else {
        this.el.forEach(e => e.addEventListener(evt, func));
      }
      return this;
    };

    this.remove = function(child) {
      if (child.el[0]) {
        this.el[0].removeChild(child.el[0]);
      }
      return this;
    };

    this.appendTo = function(node) {
      this.el[0].appendChild(node);
      return this;
    };

    this.css = function(rules) {
      let styleObj = (this.el.length) ? this.el[0].style : this.el.style;

      for (let [k, v] of Object.entries(rules)) {
        styleObj[k] = v;
      }
      return this;
    };
  }

  return (this instanceof FauxJQ) ? this.FauxJQ : new FauxJQ(el);
};

FauxJQ.createEl = el => document.createElement(el);

module.exports = FauxJQ;