'use strict';

const Gallery = require('./gallery');
const Preloader = require('./preloader');

document.addEventListener('DOMContentLoaded', function() {

  let gallery = new Gallery({
    'xs': 'col-xs-12',
    'sm': 'col-sm-6',
    'md': 'col-md-4',
    'lg': 'col-lg-3'
  });

  Preloader.preload(gallery.insertImage);
});