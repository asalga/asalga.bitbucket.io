/*

*/
const $$ = require('./fauxjq');

const Tween = require('gsap');
const TimelineMax = Tween.TimelineMax;
window.Tween = Tween;

const Preloader = {

  galleryVisible: false,

  showGallery: function() {
    if (this.galleryVisible) {
      return;
    }
    this.galleryVisible = true;

    // Fade then remove the overlay
    let overlay = document.querySelector('.overlay');
    let nav = document.querySelector('.nav');
    let navTitle = document.querySelector('.nav h1');
    let heading = document.querySelector('h1.header');


    Tween.to(heading, 0.5, {
      opacity: '1',
      onComplete: function() {
        console.log('test');
      }
    });

    var timeline = new TimelineMax();
    timeline.to(overlay, 0.5, {
        opacity: '0',
        onComplete: function() {
          $$('body')
            .css({ 'overflow-y': 'auto' })
            .remove($$('.overlay'));
        }
      }).to(nav, 0.5, {
        startAt: {
          left: '100%'
        },
        left: '0px'
      })
      .to(navTitle, 0.2, {
        delay: 0.5,
        startAt: { opacity: '0' },
        opacity: '1'
      });
  },

  preload: function(cb) {

    let self = this;
    fetch('assets/data/gallery1.json')
      .then(response => response.json())
      .then(
        json => {
          let imgCounter = 0;
          const TotalImages = json.images.length;

          json.images.forEach(function(i) {
            let img = new Image();
            let iter = i;

            img.onload = function() {
              imgCounter++;
              cb(this, i);
              if (imgCounter === TotalImages) {
                self.showGallery();
              }

            };
            img.src = i.path;
          });
        });
  }
};

module.exports = Preloader;