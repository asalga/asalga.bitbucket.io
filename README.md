
# REQ
Thinkingbox Web Developer Test

Using your knowledge of HTML, CSS, and Javascript, develop a simple webpage that displays a âgalleryâ of images in a stylized manner. 

[ ] The images should each have an associated body of text with them, and should only display this text after a user interaction.

[ ] Add a menu to this page that animates in and provides a navigational list of additional pages.

This should all be created with vanilla Javascript, HTML and CSS. You may use a Javascript library for animations.  

[ ]The page should be responsive, and look good on a large screen all the way down to a mobile device.

Not only will we judge your technical approach, but will also be looking at how creative you get. Things like styles, layout, and animations, will all be taken into consideration.

Bonus Points:

Do any one or more of these to really wow us!

Add in the ability to share the images out to one or more social networks.
Creatively add a canvas element to your page, to help bring it to life.
Add a preloader to your site to manage the loading of images and only show the page once all images have loaded.


