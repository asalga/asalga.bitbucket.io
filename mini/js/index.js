/*
 */
'use strict';

let player;

function onYouTubePlayerAPIReady() {
  player = new YT.Player('ytplayer', {
    width: '640',
    videoId: 'Ldjmb15Jsx0'
  });
}

$(document).ready(function() {
  let tag = document.createElement('script');
  tag.src = 'https://www.youtube.com/player_api';
  let firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

  $('.container.hero').on('click', function() {
    let height = $('.container.hero').css('height');
    $('#ytplayer').css({ 'display': 'block', 'height': height });
    $('.thumb').css({ 'display': 'none' });
  });

});

$(window).resize(function() {
  let w = $('.hero-parent').width();
  let aspect = 360 / 640;
  $('.hero-parent').css({ 'height': `calc(${w}px * ${aspect})` });
  $('#ytplayer').css({ 'height': '100%' });
});