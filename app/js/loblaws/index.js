/*
	Andor Salga
	March 2018
*/
'use strict';

let itemCount = 0;
let total = 0;

const GST = 0.05;
const Products = {};

const prodTemplate = $(
    `<div class='prod col-4'>
       <img class='productImg'/>
       <div class='productName'></div>
       <div class='productColor'></div>
       <div class='productPrice'></div>
       <button class='button' data-prod-id='0'>ADD</button>
     </div>`);

const productBasketTemplate = $(
    `<div class='prodBasket col-12' id='0'>
       <img class='productImg'/>
       <div class='details'>
           <div class='productName'></div>

            <!-- missing data? -->
            <div class='productBrandTitle'>
              <span class='productBrand'></span>
            </div>

            <div class='productColorTitle'>
              <span>Color: </span>
              <span class='productColor'></span>
            </div>

            <div class='productIdTitle'>
              <span>ITEM #</span>
              <span class='productId'></span>
            </div>

            <div>
              <span>QTY: </span>
               <span class='quantity'>1</span>
            </div>
        </div>
        <div class='rightSide'>
            <div class='productPrice'></div>
           <button class='button' data-prod-id=''>Remove</button>
        </div>
     </div>`);


/*
 */
function removeWithAnim(node) {
    node.animate({ height: '0' }, 200, function() {
        node.remove();
    });
}

/*
 */
function Cart() {
    this.products = new Map();

    this.add = function(prodId) {
        let count = this.products.get(prodId);
        count = count ? count + 1 : 1;
        this.products.set(prodId, count);
        this.addToBasketUi(prodId);
        this.updateTotals(prodId);
    };

    /*
    	Add component to basket
     */
    this.addToBasketUi = function(prodId) {
        let prodBasket = $('.basket').find(`div#${prodId}`);

        if (prodBasket.length === 0) {
            let data = Products[prodId];

            // Clone node and populate data
            // TODO: pretty terrible. Replace with handlebars?
            let prod = productBasketTemplate.clone();
            $(prod).attr('id', prodId);
            $(prod).find('.productImg').attr('src', data.thumbnails.b2);
            $(prod).find('.productId').html(data.productId);
            $(prod).find('.productName').html(data.productName);
            $(prod).find('.productColor').html(data.productColor);
            $(prod).find('.productPrice').html(`$${data.productPrice}`);
            $(prod).find('button')
                .attr('data-prod-id', data.productId)
                .click(btn => cart.remove(prodId));

            $('.basket .container').append(prod);
        }

        // Only need to update the quantity if already in basket
        else {
            let qty = prodBasket.find('.quantity');
            let num = parseInt(qty.html()) + 1;
            qty.html(num);
        }
    };

    /*
     */
    this.updateTotals = function() {
        let subTotal = 0;
        this.products.forEach((count, prodId) => {
            subTotal += count * Products[prodId].productPrice;
        });

        let gst = (GST * subTotal).toFixed(2);
        let total = (subTotal + GST * subTotal).toFixed(2);

        $('#orderGst').html(`$${gst}`);
        $('#orderSubTotal').html(`$${subTotal}`);
        $('#orderTotal').html(`$${total}`);
    };

    /*
     */
    this.remove = function(prodId) {
        let p = this.products.get(prodId);
        if (!p) { return; }
        this.products.set(prodId, p - 1);
        this.removeFromBasket(prodId);
        this.updateTotals();
    };

    /*
    	UI component

    	@String {prodId}
     */
    this.removeFromBasket = function(prodId) {
        let prodBasket = $('.basket').find(`div#${prodId}`);

        // sanity check
        if (prodBasket.length === 1) {
            let qtyNode = prodBasket.find('.quantity');
            let qty = parseInt(qtyNode.html(), 10);
            qty--;
            qtyNode.html(qty);

            // Remove the entire node if there's only 1 node left.
            if (qty === 0) {
                removeWithAnim(prodBasket);
            }
        }
    };
}

let cart = new Cart();


/**
	prodData - obj with price, name, color, etc.
*/
function addProductToDom(prodData) {

    let row = $('.productContainer .row').last();

    if (total % 3 === 0) {
        row = $(`<div class='row' id='${total}'/>`);
        $('.productContainer').append(row);
    }

    let prod = prodTemplate.clone();
    $(prod).find('.productImg').attr('src', prodData.thumbnails.b2);
    $(prod).find('.productName').html(prodData.productName);
    $(prod).find('.productPrice').html(`$${prodData.productPrice}`);
    $(prod).find('.productColor').html(prodData.productColor);
    $(prod).find('button')
        .attr('data-prod-id', prodData.productId)
        .click(function(btn) {
            let prodId = $(this).attr('data-prod-id');
            cart.add(prodId);
        });

    row.append(prod);
    total++;
}


$(window).ready(function() {

    fetch('data/data.json')
        .then(function(res) {
            return res.json();
        })
        .then(function(res) {
            let i = 0;
            res.results.forEach(v => {
                if (i < 6) {
                    Products[v.productId] = v;
                    addProductToDom(v);
                }
                i++;
            });
        });

    // For some reason, this doesn't work unless we put in a timeout.
    window.setTimeout(() => {
        $('.productName').matchHeight({ property: 'min-height' });
    }, 100);
});