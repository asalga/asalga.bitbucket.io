'use strict';

module.exports = function(grunt) {

  const LivereloadPort = 35729;
  const ServeStatic = require('serve-static');

  // output dirs
  const src = 'src';
  const tmp = '.tmp';
  const sassCache = '.sass-cache';
  const app = 'app';

  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

  grunt.initConfig({
    project: {},

    /** 
     * Delete all working directories and contents
     */
    clean: {
      build: {
        src: [
          `${app}`,
          `${tmp}`,
          `${sassCache}`
        ]
      }
    },

    /**
     * grunt-open
     *
     * Opens the web server in the browser
     */
    open: {
      server: {
        path: `http://localhost:<%= connect.options.port %>/`
      }
    },


    /**
     * grunt-contrib-connect
     */
    connect: {
      options: {
        port: 9000,
        hostname: '*'
      },
      livereload: {
        options: {
          middleware: function(connect, options) {
            return [
              ServeStatic(`${app}`),
              connect().use(`${app}`, ServeStatic(`${app}`)),
              ServeStatic(`${app}`)
            ]
          }
        }
      }
    },


    /**
     *
     */
    copy: {
      dev: {
        files: [
          // MARKUP
          {
            expand: true,
            src: 'index.html',
            dest: `${app}`,
            filter: 'isFile'
          },
          // STYLE
          {
            expand: true,
            cwd: `${src}/css/`,
            src: '*.css',
            dest: `${app}/assets/css/`,
            filter: 'isFile'
          },
          // DATA
          {
            expand: true,
            cwd: `${src}/data/`,
            src: '*.json',
            dest: `${app}/assets/data/`,
            filter: 'isFile'
          }
        ]
      }
    },

    /**
     * 
     */
    sass: {
      dev: {
        options: {
          style: 'nested',
          sourcemap: 'auto'
        },
        files: [{
          dest: `${app}/assets/css/style.css`,
          src: `${src}/scss/style.scss`
        }]
      }
    },

    /**
     * 
     */
    browserify: {
      dev: {
        files: [{
          dest: `${app}/assets/js/dev_bundle.js`,
          src: `${src}/js/index.js`
        }],
        options: {
          mangle: false
        }
      }
    },

    /**
     * 
     * options in .jshintrc file
     */
    jshint: {
      options: {
        jshintrc: '.jshintrc'
      },
      files: [
        'src/js/**/*.js'
      ]
    },

    /**
     *
     * https://github.com/gruntjs/grunt-contrib-watch
     *
     */
    watch: {
      options: {
        spawn: false,
        livereload: true
      },
      scripts_dev: {
        files: [
          `${src}/js/**/*.js`
        ],
        tasks: [
          'jshint',
          'copy',
          'browserify:dev'
        ],
        options: {
          livereload: true
        }
      },
      data: {
        files: [
          `${src}/data/**/*.{json,jpg}`
        ],
        tasks: [
          'copy:dev'
        ],
        options: {
          livereload: true
        }
      },
      scss: {
        files: [
          `${src}/scss/**/*.scss`,
        ],
        tasks: [
          'sass:dev'
        ],
        options: {
          livereload: true
        }
      },
      markup: {
        files: ['index.html'],
        tasks: ['copy:dev'],
        options: {
          livereload: true
        }
      }
    }
  });

  /*
    Dev build
  */
  grunt.registerTask('default', [
    'copy:dev',
    'sass:dev',
    // 'jshint',
    'browserify:dev',
    'connect:livereload',
    'open',
    'watch'
  ]);

  /*
    Distribution build
  */
  grunt.registerTask('dist', [
    'copy:dist',
    'sass:dist',
    'browserify:dist'
  ]);

};